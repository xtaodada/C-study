程序改错题：改正下列程序中的错误，在屏幕上显示短句"Welcome to you!"

提交改错后的代码文件。

```c
#include<stdio.h>

int mian(){

Printf(Welcome to you!");

return 0;

}
```

# 运行结果

成功通过编译, 且无编译警告

共有测试数据:1
平均占用内存:1.887K
平均CPU时间:0.00629S
平均墙钟时间:0.00629S

测试数据	评判结果
测试数据1	完全正确