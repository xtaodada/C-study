#include <stdio.h>
#include <string.h>

int main() {
	char originstr[10];
	scanf("%s", originstr);
	char * origin = originstr, * comma = ",", result[20];
	if(strlen(origin) > 3) {
		int pos = strlen(origin) - 3;
		strncpy(result, origin, pos);
		result[pos] = '\0';
		strcat(result, comma);
		strcat(result, origin + pos);
		printf("%s\n", result);
	}
	else {
		printf("%s\n", origin);
	}
   return 0;
}
