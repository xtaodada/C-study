【问题描述】

给定平面任意两点坐标(x1,y1)和(x2,y2),求这两点之间的距离(保留2位小数)｡要求定义和调用函数dist(x1, y1, x2,y2)计算两点间的距离｡

【输入形式】

从键盘输入点坐标(x1,y1)

从键盘输入点坐标(x2,y2)

【输入输出样例1】（下划线部分表示输入）

Input(x1,y1): 35.5 48.6

Input(x2,y2): 210.7 104.5

distance=183.90

【样例说明】

输入提示符后要加一个空格。例如Input (x1,y1): ，其中:后要加一个且只能一个空格。

输入的数据之间以一个空格相隔。

输出语句的=;两边无空格。

英文字母区分大小写。必须严格按样例输入输出。

# 运行结果

成功通过编译, 且无编译警告

共有测试数据:1
平均占用内存:2.004K
平均CPU时间:0.00576S
平均墙钟时间:0.00577S

测试数据    评判结果
测试数据1   完全正确
