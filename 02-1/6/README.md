【问题描述】计算某个时间（x时y分z秒）距离0时0分0秒有多少分钟？

【输入形式】15:20:21

【输出形式】55221


提示：输入时scanf中应该包含普通字符“:”，即“%d:%d:%d”

# 运行结果

成功通过编译, 且无编译警告

共有测试数据:3
平均占用内存:1.946K
平均CPU时间:0.00526S
平均墙钟时间:0.00527S

测试数据	评判结果
测试数据1	完全正确
测试数据2	完全正确
测试数据3	完全正确
