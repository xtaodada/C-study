【问题描述】 改正下列程序中的错误， 程序功能：求华氏温度100oF对应的摄氏温度。计算公式如下，

![1](1.png)

其中c为摄氏温度，f为华氏温度。提交改错后的代码文件。

```c
#include <stdoi.h>

Void main(){

    int celsius;fahr;

    fahr=100;

    celsius=5*(fahr-32)/9;

    printf(“fahr=d,celsius=%d\n”,fahr,celsius);

    }
```

【输入形式】无
【输出形式】fahr=100,celsius=37

# 运行结果

成功通过编译, 且无编译警告

共有测试数据:1
平均占用内存:1.902K
平均CPU时间:0.00607S
平均墙钟时间:0.00613S

测试数据	评判结果
测试数据1	完全正确
