#include<stdio.h>

int month2day(int year,int month){
	int tmp[12] = {31,28,31,30,31,30,31,31,30,31,30,31};
	
	if(((year%4==0) && (year%100!=0)) || year%400==0)tmp[1]++;
	
	return tmp[month-2];
}

int main(){
	int year,month,day;

	printf("Input year, month, day: \n");
	scanf("%d %d %d",&year,&month,&day);
	
	for(;month>1;month--){
		day += month2day(year,month);
	}
	
	printf("Days of year: %d",day);

	return 0;
}
