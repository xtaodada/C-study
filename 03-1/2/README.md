【问题描述】 2011年开始实行新个人所得税法，要求输入月薪salary，输出应交的个人所得税 tax (保留两位小数)。
```
   新税法方案如下：
       tax=rate*(salary-3500) - deduction
   当 salary<=3500时，rate=0、deduction=0;
   当 3500<salary<=5000时,rate=3%、deduction=0;
   当 5000<salary<=8000时,rate=10%、deduction=105;
   当 8000<salary<=12500时,rate=20% 、deduction=555;
   当 12500<salary<=38500时, rate=25% 、deduction=1005;
   当 38500<salary<=58500时, rate=30% 、deduction=2755;
   当 58500<salary<=83500时, rate=35% 、deduction=5505;
   当 83500<salary时, rate=45% 、deduction=13505;
```

【输入输出样例1】（下划线部分表示输入）

Enter the salary: <u>5010.87</u>
tax=46.09

【输入输出样例2】（下划线部分表示输入）

Enter the salary: <u>32098.76</u>
tax=6144.09

【输入输出样例3】（下划线部分表示输入）

Enter the salary: <u>3000</u>
tax=0.00


【输入输出样例4】（下划线部分表示输入）


Enter the salary: <u>59000</u>
tax=13920.00

【输入输出样例5】（下划线部分表示输入）

Enter the salary: <u>84500</u>
tax=22945.00

【样例说明】

输入提示符后要加一个空格。例如`Enter the salary : ` ，其中:后要加一个且只能一个空格。

输出语句的=两边无空格。

英文字母区分大小写。必须严格按样例输入输出。

# 运行结果

成功通过编译, 且无编译警告

共有测试数据:5
平均占用内存:1.996K
平均CPU时间:0.00632S
平均墙钟时间:0.00632S

测试数据	评判结果
测试数据1	完全正确
测试数据2	完全正确
测试数据3	完全正确
测试数据4	完全正确
测试数据5	完全正确
