【问题描述】 从键盘输入三角形的三条边长，判断能否构成三角形，如能构成三角形，则判断是哪一种类型：等腰三角形(等腰直角算作等腰)、等边三角形、直角三角形、任意三角形。输出要求如下：

不是三角形，则输出： It isn't triangle.

等腰三角形，则输出： isoceles triangle

等边三角形，则输出： equilateral triangle

直角三角形，则输出： right-angled triangle

任意三角形，则输出： arbitrary triangle

【输入形式】

从键盘输入三角形的三条边长(实数)。

【输出形式】

不是三角形，则输出： It isn't triangle.

等腰三角形，则输出： isoceles triangle

等边三角形，则输出： equilateral triangle

直角三角形，则输出： right-angled triangle

任意三角形，则输出： arbitrary triangle

【样例输入1】 1 2 3

【样例输出1】 It isn't triangle

【样例输入2】 3.3 4.4 5.5

【样例输出2】 right-angled triangle

【样例输入3】 9.8995 14 9.8995

【样例输出3】 isoceles triangle

# 运行结果

成功通过编译, 且无编译警告

共有测试数据:5
平均占用内存:1.965K    平均CPU时间:0.00551S    平均墙钟时间:0.00553S

测试数据 评判结果
测试数据1     完全正确
测试数据2     完全正确
测试数据3     完全正确
测试数据4     完全正确
测试数据5     完全正确
