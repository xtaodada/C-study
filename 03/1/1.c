#include <stdio.h>

int sign(int x){
	if (x < 0)
		return -1;
	else 
		if (x > 0)
			return 1;
		else 
			return x;
}

int main(){

    int x;

    printf("Enter x: ");
    scanf("%d", &x);
    printf("\nsign(%d)=%d",x,sign(x));
}
