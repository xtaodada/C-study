【问题描述】 输入x,计算并输出下列分段函数sign(x)的值｡要求定义和调用函数sign(x) 实现该分段函数｡

![1](1.png)

【输入形式】从键盘输入整数x
【输入输出样例1】（下划线部分表示输入）

Enter x: <u>10</u>

sign(10)=1

【输入输出样例2】（下划线部分表示输入）

Enter x: <u>-5</u>

sign(-5)=-1

【输入输出样例3】（下划线部分表示输入）

Enter x: <u>0</u>

sign(0)=0

【样例说明】

输入提示符后要加一个空格。例如Enter x:，其中:后要加一个且只能一个空格。

输出语句的=两边无空格。

英文字母区分大小写。必须严格按样例输入输出。

# 运行结果

成功通过编译, 且无编译警告

共有测试数据:3
平均占用内存:1.943K
平均CPU时间:0.00549S
平均墙钟时间:0.00548S

测试数据	评判结果
测试数据1   完全正确
测试数据2   完全正确
测试数据3   完全正确
