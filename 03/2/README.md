【问题描述】 编程题：输入一个形式如“操作数 运算符 操作数”的四则运算表达式，输出运算结果。

【输入形式】 “操作数 运算符 操作数”，三者之间没有任何符号，操作数可以是整数或小数，运算符为“+ - * /”共4种情况，若为其他运算符，则不计算，直接输出“无效运算符”

【输出形式】 有效运算符，输出“=结果”，结果保留2位小数；其他运算符直接输出“无效运算符”

【样例输入1】3.5+4.5

【样例输出1】=8.00

【样例输入2】5/3

【样例输出2】=1.67

【样例输入3】3^4

【样例输出3】无效运算符

# 运行结果

成功通过编译, 且无编译警告

共有测试数据:3
平均占用内存:1.982K
平均CPU时间:0.00460S
平均墙钟时间:0.00459S

测试数据	评判结果
测试数据1	完全正确
测试数据2	完全正确
测试数据3	完全正确
