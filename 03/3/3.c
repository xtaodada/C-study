#include<stdio.h>

int number(int num){
	
	int a = num % 3,b = num % 5,c = num % 7,first = 1;
	
	if (a == 0 || b == 0 || c == 0) {
	    printf("Can be divisible by ");
	    if (a == 0){
	    	first = 0;
			printf("3");
			}
	    if (b == 0){
		    if (first) {
		        printf("5");
		        first = 0;
		    }
		    else
		    	printf(",5");
		    }
		if (c == 0){
			if (first) {
				printf("7");
				first = 0;
			}
			else
				printf(",7");
		}
	}
	else {
	    printf("Can not be divisible by 3,5,7");
	}
	printf(".");
	return 0;
}

int main()
{   
	int num;
	scanf("%d",&num);
	number(num);
    return 0;
}
