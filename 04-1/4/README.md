【问题描述】

用循环的嵌套来 输出以下 `4*5` 的矩阵。

```
1	2	3	4	5	
2	4	6	8	10	
3	6	9	12	15	
4	8	12	16	20
```

【输入形式】

【输出形式】

```
1	2	3	4	5	
2	4	6	8	10	
3	6	9	12	15	
4	8	12	16	20
```

【样例输入】

【样例输出】

【样例说明】

【评分标准】

# 运行结果

成功通过编译, 且无编译警告

共有测试数据:1
平均占用内存:1.902K
平均CPU时间:0.00506S
平均墙钟时间:0.00504S

测试数据	评判结果
测试数据1	完全正确
