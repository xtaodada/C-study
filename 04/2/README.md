【问题描述】

编写一个程序，用户输入若干整数，试找出其中的最大数和最小数。

【输入形式】

用户在第一行待输入数据个数，在第二行输入数据。

【输出形式】

程序在下一行输出数据的最大值和最小值

【样例输入】

```
 5
 89 62 96 74 52
```

【样例输出】

96 52

【样例说明】

用户第一次输入的为数据个数，在下一行依次输入数据。输出为5个数中的最大值和最小值，输出时候两个数之间用空格分隔。

【5组测试数据，仅提供输入信息，其程序运行结果将与正确答案进行匹配来判分】

```
9

1 2 78 5 8 6 6 9 7



5

89 62 96 74 52



1

5



2

3 5



6

123 456 769 0 -58 24
```


# 运行结果

成功通过编译, 且无编译警告

共有测试数据:5
平均占用内存:1.942K
平均CPU时间:0.00491S
平均墙钟时间:0.00490S

测试数据	评判结果
测试数据1	完全正确
测试数据2	完全正确
测试数据3	完全正确
测试数据4	完全正确
测试数据5	完全正确
