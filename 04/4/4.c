#include<stdio.h>

int is(int number){
	int n,sum=0,old=number;
	while(1){
		n = number % 10;
		sum += n * n * n;
		number /= 10;
		if(number==0)break;
	}
	if(old==sum)return 1;
	else return 0;
}
int main(){   
	int m,n;
	
	printf("Input m: ");
	scanf("%d",&m);
	printf("Input n: ");
	scanf("%d",&n);
	
	for(int i=m;i<=n;i++){
		if(is(i))printf("\n%d",i);
	}

	return 0;
}
