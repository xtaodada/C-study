#include<stdio.h>

int get_max_hang(int a[10][10],int n,int l){
	int max=a[n][0];
	for(int i=0;i<l;i++){
		if(max<a[n][i]){
			max=a[n][i];
		}
	}
	return max;
}
int get_max_lie(int a[10][10],int n,int h){
	int max=a[0][n];
	for(int i=0;i<h;i++){
		if(max<a[i][n]){
			max=a[i][n];
		}
	}
	return max;
}
void print_value_list(int a[10][10],int n,int h,int l,int max){
	for(int i=0;i<l;i++){
		if(a[n][i]==max){
			if(a[n][i]==get_max_lie(a,i,h)){
				printf("%d %d %d\n",a[n][i],n+1,i+1);
			}
		}
	}
}
int main()
{   
	int a[10][10],n,l,max;
	scanf("%d %d",&n,&l);
	for(int i=0;i<n;i++){
		for(int j=0;j<l;j++){
			scanf("%d",&a[i][j]);
		}
	}
	
	for(int i=0;i<n;i++){
		max=get_max_hang(a,i,l);
		print_value_list(a,i,n,l,max);
	}
}
