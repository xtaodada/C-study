【问题描述】

从标准输入中输入一个N（N<=9）阶矩阵和一个M（M<=N）阶矩阵，判断矩阵M是否是N的子矩阵，若是则输出M在N中的起始位置，若不是则输出-1。若矩阵M能与N中某一区域完全相等，则称M是N的子矩阵。

【输入形式】

从标准输入读取矩阵。

第一行只有一个整数N，代表第一个矩阵的阶数。后续有N行输入，每行有N个以若干空格分隔的整数，代表该矩阵在该行上的所有元素。

输入完N阶矩阵后，再在下一行输入一个整数M，代表第二个矩阵的阶数。后续有M行输入，每行有M个以若干空格分隔的整数，代表该矩阵在该行上的所有元素。

【输出形式】

输出M在N中的起始位置，即N中的第几行第几列，两个数字用逗号“,”分隔（从第1行第1列开始计数，即：矩阵第一个元素的位置为：1,1。

若N有多个子矩阵与M矩阵完全相同，则输出首先找到的起始位置，即行最小的位置，若行相同，则为列最小的位置。

若M不是N的子矩阵，则输出-1。

【样例输入】

```
6

3        9        15     25     -9     0

36     102     2       5      67    89

8       12       58     6      53    456

67      7       895   -12   65    -83

-56    812    25     0      72     61

4       71       69    -4     341  970

3

6        53      456

-12    65      -83

0       72       61
```

【样例输出】

```
3,4
```

【样例说明】

第一个矩阵为6阶矩阵，第二个矩阵为3阶矩阵，第二个矩阵与第一个矩阵的某个子矩阵（起始位置为第3行第4列的3阶矩阵）完全相同，故输出3,4，行列数用逗号隔开。

【评分标准】

该题要求输出M矩阵在N矩阵的起始位置。上传C语言文件名为 `example2b.c` 。

# 运行结果

成功通过编译, 且无编译警告

共有测试数据:5
平均占用内存:1.949K
平均CPU时间:0.00617S
平均墙钟时间:0.00618S

测试数据	评判结果
测试数据1	完全正确
测试数据2	完全正确
测试数据3	完全正确
测试数据4	完全正确
测试数据5	完全正确
