#include <stdio.h>
 
int main()
{
    int n=30,t1=1,t2=1,t3;

    for(int i=1;i<=n;i++){
        printf("%d\t", t1);
        t3 = t1+t2;
        t1 = t2;
        t2 = t3;
        if((i%5==0)&&(i/5!=6))printf("\n");
    }
    return 0;
}
