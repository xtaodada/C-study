#include<stdio.h>

int main(){
    int n,a[10],max=0,min=0;

    scanf("%d", &n);

    for(int i=0; i<n; i++){
        scanf("%d", &a[i]);
    }

    for (int i = 0; i < n; ++i) {
        if(i==0){
            max = a[i];
            min = a[i];
        }
        if(a[i]>max){
            max = a[i];
        }
        if(a[i]<min){
            min = a[i];
        }
    }

    printf("%d %d",max,min);

    return 0;
}
