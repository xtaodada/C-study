#include<stdio.h>

int get_min(int a[20],int n,int temp){
    int min=2147483647;
    for(int i=0;i<n;i++){
        if((a[i]<min)&&(a[i]>temp)){
            min=a[i];
        }
    }
    return min;
}

int main(){
    int n,a[20],b[20],min=-2147483648;
	
	scanf("%d",&n);

    for(int i=0; i<n; i++){
        scanf("%d", &a[i]);
    }

    for(int i=1;i<=n;i++){
        min=get_min(a,n,min);
        for(int j=0;j<n;j++){
            if(a[j]==min){
                b[j]=i;
            }
        }
    }

    for(int i=0;i<n;i++){
        printf("%d ",b[i]);
    }

	return 0;
}
