#include<stdio.h>

int isPalindrome(int n) {
    int m = n;
    int r = 0;
    while (n > 0) {
        r = r * 10 + n % 10;
        n /= 10;
    }
    return r == m;
}
int main()
{   
	int a,b;
	scanf("%d %d",&a,&b);
	for(int i=a;i<=b;i++){
		if(isPalindrome(i)){
            printf("%d\n",i);
        }
	}
    return 0;
}
