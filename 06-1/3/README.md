【问题描述】

打印数字金字塔。输入层数，输出金字塔。

【输入形式】

【输出形式】

【样例输入】

```
3
```

【样例输出】

```
  1  

 2 2  

3 3 3  
```

【样例说明】

【评分标准】

# 运行结果

成功通过编译, 且无编译警告

共有测试数据:1
平均占用内存:1.953K
平均CPU时间:0.00434S
平均墙钟时间:0.00434S

测试数据	评判结果
测试数据1	完全正确
