【问题描述】

给定平面任意两点坐标（x1，y1）和（x2，y2），设计dist（x1，y1，x2，y2），求这两点之间的距离（坐标值为实数，距离值保留2位小数）

【输入形式】

一行输入两点的坐标值，一共4个实数，实数间用空格间隔；

【输出形式】

一行输出距离值，保留2位小数

【样例输入】

```
1 0 0 2
```

【样例输出】

```
2.24
```

【样例说明】

【评分标准】

# 运行结果

成功通过编译, 且无编译警告

共有测试数据:5
平均占用内存:1.996K
平均CPU时间:0.00505S
平均墙钟时间:0.00506S

测试数据	评判结果
测试数据1	完全正确
测试数据2	完全正确
测试数据3	完全正确
测试数据4	完全正确
测试数据5	完全正确
