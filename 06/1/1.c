#include<stdio.h>

int sign(float x){
    if(x<0)return -1;
    if(x>0)return 1;
    return 0;
}
int main(){
    float x;

    while(scanf("%f", &x)!=EOF){
        printf("%d ", sign(x));
    }

    return 0;
}
