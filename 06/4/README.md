【问题描述】

编写一个函数sum(i),将输入的整形参数i（i≥0）的各位求和，返回求和结果。在main()函数中测试此函数，从键盘输入整数n（n∈[1,20]），然后再输入n个非负整数，对于每一个非负整数调用sum函数求各位数和，将结果输出到屏幕。

【输入形式】

从键盘输入一个正整数n，然后再输入n个非负整数。

【输出形式】

在屏幕上分别输出n个非负整数的各位之和，并用一个空格隔开各个整数的和。

【输入样例】

```
4
234567
0
999999
000432
```

【输出样例】

```
27#0#54#9
```

【样例说明】

整数234567、0、999999和000432各位之和（个位、十位、百位……相加）分别为：27、0、54和9，故在屏幕上打印输出：27#0#54#9
注："#"代表空格，结果之间用一个空格隔开。

【评分标准】

结果完全正确得20分，每个测试点4分，提交程序文件名称为 `c31.c` 。

# 运行结果

成功通过编译, 且无编译警告

共有测试数据:5
平均占用内存:1.947K
平均CPU时间:0.00535S
平均墙钟时间:0.00538S

测试数据	评判结果
测试数据1	完全正确
测试数据2	完全正确
测试数据3	完全正确
测试数据4	完全正确
测试数据5	完全正确
