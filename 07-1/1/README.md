【问题描述】

从标准输入中输入两组整数(每行不超过20个整数，每组整数中元素不重复),合并两组整数，去掉在两组整数中都出现的整数，并按从小到大顺序排序输出（即两组整数集“异或”）。

【输入形式】

首先输入第一组整数的个数，再输入第一组整数，以空格分隔；然后输入第二组整数的个数，再输入第二组整数，以空格分隔。

【输出形式】

按从小到大顺序排序输出合并后的整数（去掉在两组整数中都出现的整数），输出整数间以一个空格分隔，最后不含回车符。

【样例输入】

```
8
5 1 4 3 8 7 9 6
4
5 2 8 10
```

【样例输出】

```
1 2 3 4 6 7 9 10
```

【样例说明】

第一组整数个数为8，分别为5 1 4 3 8 7 9 6，第二组整数个数为4，分别为5 2 8 10。将第一组和第二组整数合并（去掉在两组整数中都出现的整数），并从小到大顺序排序后结果为1 2 3 4 6 7 9 10。

【评分标准】

结果完全正确得25分，每个测试点5分，提交程序文件名为 `exam2.c` 。

# 运行结果

成功通过编译, 且无编译警告

共有测试数据:5
平均占用内存:1.943K
平均CPU时间:0.00562S
平均墙钟时间:0.00564S

测试数据	评判结果
测试数据1	完全正确
测试数据2	完全正确
测试数据3	完全正确
测试数据4	完全正确
测试数据5	完全正确
