#include<stdio.h>

int get_h_min(int a[10][10], int h, int l){
	int temp=a[h][0];
	for(int i=1;i<l;i++){
		if(a[h][i]<temp){
			temp = a[h][i];
		}
	}
    return temp;
}
int get_l_min(int a[10][10], int l, int h){
	int temp=a[0][l];
	for(int i=1;i<h;i++){
		if(a[i][l]<temp){
			temp = a[i][l];
		}
	}
    return temp;
}
int main(){
    int a[10][10],h,l,temp;
    scanf("%d %d",&h,&l);
    
    for(int i=0;i<h;i++){
		for (int j=0;j<l;j++){
			scanf("%d",&a[i][j]);
		}
	}

    for(int i=0;i<h;i++){
		temp=get_h_min(a,i,l);
		for(int j=0;j<l;j++){
			if(temp==a[i][j]){
				if(temp==get_l_min(a,j,h)){
					printf("%d %d %d\n",temp,i+1,j+1);
				}
			}
		}
	}
    return 0;
}
