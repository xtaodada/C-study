# 实验七 指针(2学时)

> 作业时间： 2021-12-06 19:12:00 至 2021-12-26 23:55:00

# 实验目的

1. 掌握指针的概念、会定义和使用指针变量；
2. 掌握通过指针访问单个变量的方法；
3. 了解指针指向数组的使用。

# 实验器材

计算机硬件环境：PIII 667以上计算机；软件环境：Visual C，Devc++。

# 技能要点

1. 指针的定义，初始化，引用；
2. `＆`和`*`运算符的使用；
3. 指向数组的指针的定义，初始化，引用；
4. 指针的比较运算（例如：p>q）;
5. 指针与整数的加减运算（例如：p+i）;
6. 使用运算符`[]`，实现用下标法间接访问数组；
7. 使用运算符`*`和`(+` 或 `-)`，实现用指针法间接访问数组。

# 思考题

1.    指针做赋值运算时，赋值运算符右侧可以是那些值？
答：第一，```int x,*p,*q;
       p=&x; 同类型变量的地址
       q=p;  同类型指针变量```

    第二，```int a[10],*p,*q;
       p=a; 数组名
       p=&a[0]； p=a+0; 数组元素地址
       q=p;  同类型指针变量```
