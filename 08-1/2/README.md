【问题描述】

编写一个函数invert(s)，颠倒输入的字符串参数s后返回。在main()函数中测试此函数：从键盘输入一个正整数n`（n∈[1,20]）`,然后再输入n个字符串（长度不大于100），对于每一个字符串，然后调用invert函数进行颠倒字符串操作，将颠倒后的字符串打印在屏幕上。

【输入形式】

从键盘输入整数n，然后再输入n个字符串，每行一个字符串。

【输出形式】

输出各个字符串颠倒以后的字符串，每个字符串之间用换行符隔开。

【输入样例】

```
3
1AdkflsddfYjkdfDD
aaaaaaaaaaAAAAAAAAAA
12345678aBc
```

【输出样例】

```
DDfdkjYfddslfkdA1
AAAAAAAAAAaaaaaaaaaa
cBa87654321
```
【样例说明】

因为输入n为3，故输入3个字符串。将字符串"1AdkflsddfYjkdfDD"首尾颠倒后的结果是"DDfdkjYfddslfkdA1"；将字符串"aaaaaaaaaaAAAAAAAAAA"首尾颠倒后的结果是"AAAAAAAAAAaaaaaaaaaa"；将字符串"12345678aBc"首尾颠倒后的结果是"cBa87654321"；故输出：
DDfdkjYfddslfkdA1
AAAAAAAAAAaaaaaaaaaa
cBa87654321

【评分标准】

结果完全正确得20分，每个测试点4分，提交程序文件名称为 `c11.c` 。

# 运行结果

成功通过编译, 且无编译警告

共有测试数据:5
平均占用内存:1.934K
平均CPU时间:0.00524S
平均墙钟时间:0.00524S

测试数据	评判结果
测试数据1	完全正确
测试数据2	完全正确
测试数据3	完全正确
测试数据4	完全正确
测试数据5	完全正确
