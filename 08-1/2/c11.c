#include<stdio.h>
#include<string.h>

void invert(char s[], int len){
	char c;
	for(int i=0;i<len/2;i++){
		c=s[len-i-1];
		s[len-i-1]=s[i];
		s[i]=c;
	}
}
int main(){
    int n;
    char s[101];
    scanf("%d",&n);
    for(int i=0;i<n;i++){
		scanf("%s",s);
		invert(s, strlen(s));
		printf("%s\n",s);
	}
	return 0;
}
