#include<stdio.h>

typedef struct Stu{
	char name[20];
	int score;
	int age;
}stu;

int main(){
    stu data[5];
    // 输入
	for(int i=0;i<5;i++){
		scanf("%s%d%d",data[i].name,&data[i].age,&data[i].score);
	}

	printf("成绩变化之前：\n");
	// 输出
	for(int i=0;i<5;i++){
		printf("%-8s%-6d%d\n",data[i].name,data[i].age,data[i].score);	
	}
	// 改变
	for(int i=0;i<5;i++){
		if(data[i].age>18){
			data[i].score+=10;	
		}	
	}
	
	printf("成绩变化之后：\n");
	// 输出
	for(int i=0;i<5;i++){
		printf("%-8s%-6d%d\n",data[i].name,data[i].age,data[i].score);	
	}

    return 0;
}
