#include<stdio.h>
#define N 3

typedef struct student{
    char num[6];
    char name[8];
    int score[3];
    float avr;
}stu;

int main(){
    stu data[N];
    float temp;
    // 输入
    for(int i=0;i<N;i++){
		scanf("%s\n%s\n%d\n%d\n%d", data[i].num, data[i].name,&data[i].score[0],&data[i].score[1],&data[i].score[2]);
	}
	// 求平均
	for(int i=0;i<N;i++){
		temp = data[i].score[0] + data[i].score[1] + data[i].score[2];
		data[i].avr = temp / 3;
	}
	// 输出
	printf("NO.    name    score1    score2    score3    average\n");
	for(int i=0;i<N;i++){
		printf("%-7s%-9s%-10d%-9d%-10d%.2lf\n",data[i].num,data[i].name,data[i].score[0],data[i].score[1],data[i].score[2],data[i].avr);	
	}
    return 0;
}
