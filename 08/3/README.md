【问题描述】

有3个学生，每个学生的数据包括学号，姓名，3门课的成绩，从键盘输入3个学生的数据，要求打印出3门课的平均成绩.（包括学号，姓名，3门课成绩，平均分数）。

```
# define N 3

struct student

{char num[6];

 char name[8];

 int score[3];

 float avr;

}stu [N];
```

【输出形式】

【样例输入】

```
Input scores of student 1:

NO.: 101

name: Wang

score1: 93

score2: 89

score3: 87

 

Input scores of student 2:

NO.: 102

name: Li

score1: 85

score2: 80

score3: 78

 

Input scores of student 3:

NO.: 103

name: Zhao

score1: 65

score2: 70

score3: 59
```

【样例输出】

```
NO.    name    score1    score2    score3    average

 101    Wang     93        89           87          89.67

 102     Li           85        80           78          81.00

 103    Zhao      65        70            59          64.67
```

【样例说明】

输出格式 `"%-7s%-9s%-10d%-9d%-10d%.2lf\n"`

# 运行结果

成功通过编译, 且无编译警告

共有测试数据:1
平均占用内存:1.996K
平均CPU时间:0.00601S
平均墙钟时间:0.00602S

测试数据	评判结果
测试数据1	完全正确
