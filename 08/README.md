# 实验八 结构体(2学时)

> 作业时间： 2021-12-20 08:06:00 至 2021-12-27 00:00:00

# 实验目的

1. 掌握结构体类型和结构体类型变量的定义方法。
2. 掌握结构体类型变量成员赋值和引用方法。
3. 学会使用结构体数组。
4. 理解指针和结构的关系。

# 实验器材

计算机硬件环境：PIII 667以上计算机；软件环境：Turbo C, Visual C。

# 实验内容

1. 编程题：时间换算。用结构体表示时间（时：分：秒），输入一个时间数值，再输入一个秒数n（n<60），以时：分：秒的格式输出该时间再过n秒后的时间。
输入输出示例：
输入时间：11：59：40
输入秒：30
新时间：12：0：10
2. 定义一个含姓名、年龄、英语成绩的结构体类型，通过键盘输入5个学生的信息，再对年龄大于18岁的学生的英语成绩加上10分，然后分别输出成绩变化之前和之后的所有学生的信息。
3. 有10个学生，每个学生的数据包括学号，姓名，3门课的成绩，从键盘输入10个学生的数据，要求打印出3门课的总平均成绩，以及最高分的学生的数据（包括学号，姓名，3门课成绩，平均分数）。

# 技能要点

1. 结构体类型的定义方法；
2. 结构体变量的定义方法；
3. 结构体变量的赋值，以及结构体成员的访问。

# 思考题

1. 结构体和数组都是构造类型，它们的区别是什么？

# 根据实验过程填写下列内容

1. 写出能够完成实验1要求的程序,及测试数据。
