#include<stdio.h>

typedef struct stu{
	char name[20];
	int num;
	float score;
}stu;

int main(){
    stu data[2];
    
	for(int i=0;i<2;i++){
		scanf("%d %s %f",&data[i].num,data[i].name,&data[i].score);
	}
	
	printf("The higher score is:\n");
	
	if(data[0].score>=data[1].score){
		printf("%d %s %.2f\n",data[0].num,data[0].name,data[0].score);
	}
	if(data[0].score<=data[1].score){
		printf("%d %s %.2f\n",data[1].num,data[1].name,data[1].score);
	}

	return 0;
}
