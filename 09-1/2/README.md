【问题描述】

有若干个人员的数据，其中有学生和教师。学生的数据中包括：姓名、号码、性别、职业、班级。教师的数据包括：姓名、号码、性别、职业、职务。要求用同一个表格来处理。

```
union  Categ                      

   { int clas;

      char position[10]; 

   };

struct                         

{ int num;

   char name[10];

   char sex;

   char job;

   union  Categ  category;

}person[2]; 
```

【样例输入】

```
101 

wang

m

s

501



102

sun

f

t

professor
```

【样例输出】

```
101 wang m s 501       

102 li s t professor
```

【样例说明】

【评分标准】

# 运行结果

成功通过编译, 且无编译警告

共有测试数据:1
平均占用内存:1.953K
平均CPU时间:0.00671S
平均墙钟时间:0.00671S

测试数据	评判结果
测试数据1	完全正确
